'use strict';
{
	let mongo = require('mongodb');
	let monk = require('monk');
	
	let config = require('./config');
	
	let IRCClient = require('./ircClient').IRCClient;
	let Faces = require('./MessageHandlers/faces').Faces;
	let Lols = require('./MessageHandlers/lols').Lols;
	let Admin = require('./MessageHandlers/admin').Admin;
	let Logger = require('./MessageHandlers/logger').Logger;
	let Seen = require('./MessageHandlers/seen').Seen;
	let Partyhard = require('./MessageHandlers/partyhard').Partyhard;
	let YesNo = require('./MessageHandlers/yesno').YesNo;
	let GPlus = require('./MessageHandlers/gplus').GPlus;
	let Messages = require('./MessageHandlers/messages').Messages;
	let Choose = require('./MessageHandlers/choose').Choose;
	
	
	class Bot {
		constructor() {
			let db = monk(config.mongodbConnectionString);
			
			let messageHandlers = [];
			messageHandlers.push(new Faces(db));
			messageHandlers.push(new Lols(db));
			messageHandlers.push(new Admin());
			messageHandlers.push(new Logger(db));
			messageHandlers.push(new Seen(db));
			messageHandlers.push(new Partyhard());
			messageHandlers.push(new YesNo());
			messageHandlers.push(new Choose());
			messageHandlers.push(new GPlus(db));
			messageHandlers.push(new Messages(db));
			
			this.client = new IRCClient(config.irc.server, config.irc.name, config.irc.channels, messageHandlers);
		}
	}
	
	let bot = new Bot();
}
