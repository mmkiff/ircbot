'use strict';
{
	class Seen {
		constructor(db) {
			this.db = db;
		}
		
		handleMessage(client, from, to, message) {
			if(message.startsWith('!seen '))
				this.getLastSeen(client, from, to, message);
		}
		
		getLastSeen(client, from, to, message) {
			
			
			var who = message.replace('!seen ', '').trim();
			if(who == '')
				return;
			
			console.log('Looking for ' + who);
			
			let collection = this.db.get('logs');	
			collection.find( { type: 'message', lowercaseFrom: who.toLowerCase(), to: to }, // TO DO - should also include joins
							 { limit : 1, sort : { time : -1 } }
			).then((docs) => {	
				if(docs.length === 0) 
					client.say(to, "Can't find " + who);
				else {
					let timeSince = this.sinceDate(docs[0].time);
					client.say(to, `${timeSince} <${docs[0].from}> ${docs[0].message}`);
				}
			});
		}
		
		// TO DO - clean this up?
		// this code was from macAnthony. May be able to simplify it by using some sort of date library
		// at the very least, clean it up to remove the extra spaces it inserts
		sinceDate( compareDate ) { 
			var now = new Date();
			var originalDate = new Date(compareDate);
			var since = Math.floor(( now - originalDate )/1000);
			var ret = "just now ";
			var days = 0, hours = 0, minutes = 0, seconds = 0;
			var daySeconds = 86400, hourSeconds = 3600, minuteSeconds = 60;
			var showMinutes = true;
			if( since > 0) {
				ret = "";
				if( since > daySeconds ) {
					days = Math.floor(since/daySeconds);
					since = since - (daySeconds*days);
					ret = ret + ' ' + days + ' days';
					showMinutes = false;
				}
				if( since > hourSeconds ) {
					hours = Math.floor(since/hourSeconds);
					since = since - (hourSeconds * hours);
					ret = ret + ' ' + hours + ' hours';
				}
				if( since > minuteSeconds && showMinutes ) {
					minutes = Math.floor(since/minuteSeconds);
					since = since - (minuteSeconds * minutes);
					ret = ret + ' ' + minutes + ' minutes';
				} else if( since > 0 && showMinutes ) {
					ret = ret + ' ' + since + ' seconds';
				}
				ret = ret + ' ago';
			}
			return ret;
		}
		
	}
	
	exports.Seen = Seen;
}




