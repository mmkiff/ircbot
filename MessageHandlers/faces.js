'use strict';
{
	class Faces {
		constructor(db) {
			this.db = db;
			this.lastFace = null;
		}
		
		handleMessage(client, from, to, message) {
			if(message.startsWith('!addface '))
				this.addFace(client, from, to, message);
			else if(message.startsWith('!face'))
				this.getFace(client, from, to, message);
			else if(message.startsWith('!lastface'))
				this.showFace(client, to, this.lastFace, true);
			else if(message.startsWith('!deleteface'))
				this.deleteFace(client, from, to, message);
		}
		
		deleteFace(client, from, to, message) {
			let collection = this.db.get('faces');
			
			let id = message.replace('!deleteface ', '').trim();
			
			if(id != "") {
				collection.remove({ _id: id });
				console.log('Deleted face: ' + id);
				client.say(to, 'Deleted');
			}
		}
	
		addFace(client, from, to, message) {
			let face = message.replace('!addface ', '').trim();
			
			console.log('adding face: ' + face);	
			
			let collection = this.db.get('faces');
			collection.insert({ 
				'face': face,
				'insertDate': new Date(),
				'insertUser': from,
				'insertChannel': to
			}, function (err, doc) {
				if (err) {
					console.log('Error adding face: ' + err);
					client.say(to, 'DB Error :(');
				}
				else {
					console.log('Added face');
					client.say(to, ':)');
				}
			});			
		}
		
		getFace(client, from, to, message) {
			let searchText = message.replace('!face', '').trim();
			
			let collection = this.db.get('faces');

			if(searchText === '')
				this.getRandomFace(client, to, collection);
			else
				this.searchFaces(client, to, searchText, collection);
		}
		
		getRandomFace(client, to, collection) {
			console.log('Getting a random face');
			
			collection.aggregate( { $sample: { size: 1} }).then((docs) => { 
				if(docs.length > 0)
					this.showFace(client, to, docs[0], false);
				else
					client.say(to, 'Nothing found :(');
			});
		}
		
		searchFaces(client, to, searchText, collection) {
			console.log('getting face for search text "' + searchText + '".');
			
			collection.find({ face: { $regex: this.escapeRegEx(searchText) } }).then((docs) => {
				console.log(docs.length);
				if(docs.length > 0) {
					let face = docs[Math.floor(Math.random() * docs.length)];
					this.showFace(client, to, face, false);
				}
				else
					client.say(to, 'Nothing found :(');				
			});
		}
		
		escapeRegEx(str) {
			return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"); // TO DO - move somewhere everything can use it
		}
		
		showFace(client, to, face, showId) {
			if(face != null) {
				this.lastFace = face;
				console.log(face.face);
				let metadata = '';
				if(showId)
					metadata = `(${face._id} by ${face.insertUser}) `;
				client.say(to, metadata + face.face);
			}
		}
	}
	
	exports.Faces = Faces;
}




