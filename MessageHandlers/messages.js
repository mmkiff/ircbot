'use strict';
{
	// Ideas to improve:
	// * Some people idle a lot rather than quitting and rejoining. Instead of only sending notifications when someone joins, it would be nice if izz sent a message when the person sends a message (and then sleeps on it for a bit so not to spam them)
	
	class Messages {
		constructor(db) {
			this.db = db;
		}
		
		handleMessage(client, from, to, message) {
			if(message.startsWith('!msg '))
				this.sendMessage(client, from, to, message);
			else if(message.startsWith('!msgs'))
				this.getMessages(client, from, to, message);
		}
		
		handleJoin(client, channel, nick, message) {  
			console.log('Checking if ' + nick.toLowerCase() + ' has new messages...');
			let collection = this.db.get('messages');
			collection.find({ sentTo: nick.toLowerCase() }).then((docs) => {
				if(docs.length > 0) {
					console.log('User ' + nick + ' has ' + docs.length + ' new messages.');
					client.say(nick, "You have " + docs.length + " new messages. Type !msgs to see them.");
				}
				else
					console.log('No messages for ' + nick);
			});
		}
		
		getMessages(client, from, to, message) {		
			console.log('Looking for messages for ' + from);
			
			let collection = this.db.get('messages');
			collection.find({ sentTo: from.toLowerCase() }).then((docs) => {
				if(docs.length > 0) {
					for(var i = 0; i < docs.length; i++)
						client.say(from, "[" + docs[i].insertDate.toISOString() + "] " + docs[i].insertUser + ": " + docs[i].message);
					this.deleteMessages(from.toLowerCase());
				}
				else
					client.say(from, 'No messages.');				
			});
		}
		
		deleteMessages(user) {
			console.log('deleting messages for ' + user);
			
			let collection = this.db.get('messages');			
			collection.remove({ sentTo: user });
			console.log('Deleted messages for user ' + user);
		}
		
		sendMessage(client, from, to, message) {
			var params = message.replace('!msg ', '').trim().split(' ');
			if(params.length < 2)
				return;
			
			var sendTo = params[0].toLowerCase();
			var message = message.slice(6 + sendTo.length);
			
			console.log('Saving message for ' + sendTo);
			
			if(to[0] != '#')
				to = from;
			
			var msg = {
				message: message,
				sentTo: sendTo,
				insertDate: new Date(),
				insertUser: from,
				insertChannel: to				
			};
			
			let collection = this.db.get('messages');
			collection.insert(msg, function (err, doc) {
				if (err) {
					console.log('Error adding message: ' + err);
					client.say(to, 'DB Error :(');
				}
				else {
					console.log('Added message');
					client.say(to, 'Your message has been sent.');
				}
			});			
		}
	}
	
	exports.Messages = Messages;
}




