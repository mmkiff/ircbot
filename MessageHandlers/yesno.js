'use strict';
{
	class YesNo {
		constructor(db) {
			
		}
		
		handleMessage(client, from, to, message) {
			if(message.startsWith('!yn '))
				this.answerQuestion(client, from, to, message);
		}
		
		answerQuestion(client, from, to, message) {
			let answers = ["Yes.", "No.", "Maybe.", "Definitely.", "Definitely not.", "Outlook foggy. Ask again later.", "Yeah", "Nah", "Sure", "lol no way", "I think you already know the answer."];
			var answer = answers[Math.floor(Math.random() * answers.length)];
			
			client.say(to, answer);
		}
	}
	
	exports.YesNo = YesNo;
}




