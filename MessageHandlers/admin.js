'use strict';
{
	class Admin {
		constructor() {
		}
		
		handleMessage(client, from, to, message) {
			if(message.startsWith('!say '))
				this.say(client, from, to, message);
			else if (message.startsWith('!test'))
				client.say(to, 'hi!!');
			else if (message.startsWith('!quit'))
				process.exit();
		}
		
		say(client, from, to, message) {
			message = message.replace('!say ', '');
			let splitMessage = message.split(' ');
			if(splitMessage.length > 1) {
				let channel = splitMessage[0];		
				message = message.replace(channel + ' ', '');
				client.say(channel, message);
			}			
		}
	}
	
	exports.Admin = Admin;
}




