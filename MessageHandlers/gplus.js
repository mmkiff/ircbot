'use strict';
{
	class GPlus {
		constructor(db) {
			this.db = db;
		}
		
		handleMessage(client, from, to, message) {
			if(message.startsWith('!g+'))
				this.getGPlus(client, from, to, message);
			else if(message.startsWith('!setg+'))
				this.setGPlus(client, from, to, message);
		}
	
		setGPlus(client, from, to, message) {
			let newValue = message.replace('!setg+', '').trim();
			let collection = this.db.get('settings');
			collection.update({ 
				'key': 'g+',
			}, {
				'key': 'g+',
				'value': newValue
			}, {
				upsert: true
			}, function (err, doc) {
				if (err) {
					console.log('Error adding g+ link: ' + err);
					client.say(to, 'DB Error :(');
				}
				else {
					console.log('Added g+ link');
					client.say(to, ':)');
				}
			});			
		}
		
		getGPlus(client, from, to, message) {
			console.log('getting g+ link');
			let collection = this.db.get('settings');

			collection.find({ key: 'g+' }).then((docs) => {
				if(docs.length > 0)
					client.say(to, docs[0].value);
				else
					client.say(to, 'Nothing found :(');				
			});
		}
	}
	
	exports.GPlus = GPlus;
}




