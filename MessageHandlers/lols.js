'use strict';
{
	class Lols {
		constructor(db) {
			this.db = db;
			this.lastLol = null;
		}
		
		handleMessage(client, from, to, message) {
			if(message.startsWith('!addlol '))
				this.addLol(client, from, to, message);
			else if(message.startsWith('!lol'))
				this.getLol(client, from, to, message);
			else if(message.startsWith('!lastlol'))
				this.showLol(client, to, this.lastLol, true);
			else if(message.startsWith('!deletelol'))
				this.deleteLol(client, from, to, message);
		}
		
		deleteLol(client, from, to, message) {
			let collection = this.db.get('lols');
			
			let id = message.replace('!deletelol ', '').trim();
			
			if(id != "") {
				collection.remove({ _id: id });
				console.log('Deleted lol: ' + id);
				client.say(to, 'Deleted');
			}
		}
	
		addLol(client, from, to, message) {
			let lol = message.replace('!addlol ', '').trim(); // TO DO - add extra validation. Should be link then text. Maybe store it that way too
			
			console.log('adding lol: ' + lol);	
			
			let collection = this.db.get('lols');
			collection.insert({ 
				'lol': lol,
				'insertDate': new Date(),
				'insertUser': from,
				'insertChannel': to
			}, function (err, doc) {
				if (err) {
					console.log('Error adding lol: ' + err);
					client.say(to, 'DB Error :(');
				}
				else {
					console.log('Added lol');
					client.say(to, ':)');
				}
			});			
		}
		
		getLol(client, from, to, message) {
			let searchText = message.replace('!lol', '').trim();
			
			let collection = this.db.get('lols');

			if(searchText === '')
				this.getRandomLol(client, to, collection);
			else
				this.searchLols(client, to, searchText, collection);
		}
		
		getRandomLol(client, to, collection) {
			console.log('Getting a random lol');
			
			collection.aggregate( { $sample: { size: 1} }).then((docs) => { 
				if(docs.length > 0)
					this.showLol(client, to, docs[0], false);
				else
					client.say(to, 'Nothing found :(');
			});
		}
		
		searchLols(client, to, searchText, collection) {
			console.log('getting lol for search text "' + searchText + '".');
			
			collection.find({ lol: { $regex: this.escapeRegEx(searchText) } }).then((docs) => {
				console.log(docs.length);
				if(docs.length > 0) {
					let lol = docs[Math.floor(Math.random() * docs.length)];
					this.showLol(client, to, lol, false);
				}
				else
					client.say(to, 'Nothing found :(');				
			});
		}
		
		escapeRegEx(str) {
			return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"); // TO DO - move somewhere everything can use it
		}
		
		showLol(client, to, lol, showId) {
			if(lol != null) {
				this.lastLol = lol;
				console.log(lol.lol);
				let metadata = '';
				if(showId)
					metadata = `(${lol._id} by ${lol.insertUser}) `;
				client.say(to, metadata + lol.lol);
			}
		}
	}
	
	exports.Lols = Lols;
}




