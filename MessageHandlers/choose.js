'use strict';
{
	class Choose {
		constructor(db) {
			
		}
		
		handleMessage(client, from, to, message) {
			if(message.startsWith('!choose '))
				this.makeAChoice(client, from, to, message);
		}
		
		makeAChoice(client, from, to, message) {
			let choices = message.slice(8).split(" ");
			var answer = choices[Math.floor(Math.random() * choices.length)];
			
			client.say(to, answer);
		}
	}
	
	exports.Choose = Choose;
}




